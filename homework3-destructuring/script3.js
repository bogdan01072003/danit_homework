const user1 = {
    name: "John",
    years: 30
};

const { name, years, isAdmin = false } = user1;

console.log("Ім'я:", name);
console.log("Вік:", years);
console.log("isAdmin:", isAdmin);
