const books = [
    {
        name: 'Harry Potter',
        author: 'J.K. Rowling'
    },
    {
        name: 'Lord of the Rings',
        author: 'J.R.R. Tolkien'
    },
    {
        name: 'The Witcher',
        author: 'Andrzej Sapkowski'
    }
];

const bookToAdd = {
    name: 'Game of Thrones',
    author: 'George R. R. Martin'
};

const newBooks = [...books, bookToAdd];

console.log(newBooks);
