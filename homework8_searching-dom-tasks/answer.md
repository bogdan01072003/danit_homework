## Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
   * це інтерфейс, який надає програмам можливість динамічно змінювати структуру, зміст і стилі веб-сторінок
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
    * innerHTML - це властивість, яка містить HTML-код всередині елемента.
    * innerText - це властивість, яка містить тільки текстовий вміст елемента без HTML.
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
    *  document.getElementById('elementId')
    *  document.querySelector('selector')
    *  document.querySelectorAll('selector')