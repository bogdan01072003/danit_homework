const paragraphs = document.querySelectorAll('p');
paragraphs.forEach(paragraph => {
    paragraph.style.backgroundColor = '#ff0000';
});
const optionsList = document.getElementById('optionsList');
console.log(optionsList);
const parentElement = optionsList.parentElement;
console.log(parentElement);
if (optionsList.childNodes.length > 0) {
    optionsList.childNodes.forEach(node => {
        console.log(node.nodeName, node.nodeType);
    });
}
const testParagraph = document.getElementById('testParagraph');
if (testParagraph) {
    testParagraph.textContent = 'This is a paragraph';
} else {
    console.error('Елемент з id="testParagraph" не знайдено');
}
const mainHeaderItems = document.querySelectorAll('.main-header > *');
mainHeaderItems.forEach(item => {
    item.classList.add('nav-item');
});
const sectionTitles = document.querySelectorAll('.section-title');
sectionTitles.forEach(title => {
    title.classList.remove('section-title');
});
