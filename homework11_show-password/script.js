document.addEventListener('DOMContentLoaded', function() {
    const passwordFields = document.querySelectorAll('.password-form input[type="password"]');
    const showPasswordIcons = document.querySelectorAll('.icon-password');

    showPasswordIcons.forEach(function(icon, index) {
        icon.addEventListener('click', function() {
            const passwordField = passwordFields[index];
            if (passwordField.type === 'password') {
                passwordField.type = 'text';
                icon.classList.remove('fa-eye');
                icon.classList.add('fa-eye-slash');
                icon.previousElementSibling.textContent = 'Скрыть пароль';
            } else {
                passwordField.type = 'password';
                icon.classList.remove('fa-eye-slash');
                icon.classList.add('fa-eye');
                icon.previousElementSibling.textContent = 'Показать пароль';
            }
        });
    });

    document.querySelector('.password-form').addEventListener('submit', function(event) {
        event.preventDefault();

        const firstPassword = passwordFields[0].value;
        const secondPassword = passwordFields[1].value;

        if (firstPassword === secondPassword) {
            alert('You are welcome');
        } else {
            const errorMessage = document.createElement('p');
            errorMessage.textContent = 'Потрібно ввести однакові значення';
            errorMessage.style.color = 'red';

            const existingError = document.querySelector('.error-message');
            if (existingError) {
                existingError.remove();
            }

            document.querySelector('.password-form').appendChild(errorMessage);
            errorMessage.classList.add('error-message');
        }
    });
});
