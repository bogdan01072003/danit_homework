function createNewUser() {
    let newUser = {};

    let firstName = prompt("Введіть ім'я:");
    let lastName = prompt("Введіть прізвище:");
    let birthDate = prompt("Введіть дату народження (у форматі dd.mm.yyyy):");

    newUser.getFirstName = function() {
        return firstName;
    };

    newUser.getLastName = function() {
        return lastName;
    };

    newUser.setFirstName = function(newFirstName) {
        firstName = newFirstName;
    };

    newUser.setLastName = function(newLastName) {
        lastName = newLastName;
    };

    newUser.getAge = function() {
        let today = new Date();
        let birthDateParts = birthDate.split(".");
        let birthYear = parseInt(birthDateParts[2]);
        let birthMonth = parseInt(birthDateParts[1]) - 1; 
        let birthDay = parseInt(birthDateParts[0]);
        let age = today.getFullYear() - birthYear;

        if (today.getMonth() < birthMonth || (today.getMonth() === birthMonth && today.getDate() < birthDay)) {
            age--; 
        }

        return age;
    };

    newUser.getPassword = function() {
        let firstNameInitial = firstName.charAt(0).toUpperCase();
        let lastNameLower = lastName.toLowerCase();
        let birthYear = birthDate.split(".")[2];
        return firstNameInitial + lastNameLower + birthYear;
    };

    return newUser;
}

let user = createNewUser();
console.log(user);
console.log("Вік користувача: " + user.getAge() + " років");
console.log("Пароль користувача: " + user.getPassword());
