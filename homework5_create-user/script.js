function createNewUser() {
    const newUser = {};

    let firstName = prompt("Введіть ім'я:");
    let lastName = prompt("Введіть прізвище:");

    newUser.getFirstName = function() {
        return firstName;
    };

    newUser.getLastName = function() {
        return lastName;
    };

    newUser.setFirstName = function(newFirstName) {
        firstName = newFirstName;
    };

    newUser.setLastName = function(newLastName) {
        lastName = newLastName;
    };

    newUser.getLogin = function() {
        const firstNameInitial = firstName.charAt(0).toLowerCase();
        const lastNameLower = lastName.toLowerCase();
        return firstNameInitial + lastNameLower;
    };

    return newUser;
}

const user = createNewUser();

console.log("Ім'я користувача:", user.getFirstName());
console.log("Прізвище користувача:", user.getLastName());

user.setFirstName("Ivan");
user.setLastName("Kravchenko");

console.log("Оновлене ім'я користувача:", user.getFirstName());
console.log("Оновлене прізвище користувача:", user.getLastName());

const login = user.getLogin();
console.log("Логін користувача:", login);
