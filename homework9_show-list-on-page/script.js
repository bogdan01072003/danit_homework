function createList(array, element = document.body) {
    const ul = document.createElement('ul');

    array.forEach(item => {
        const li = document.createElement('li');
        if (Array.isArray(item)) {
            const nestedUl = createList(item, li);
            li.appendChild(nestedUl);
        } else {
            li.textContent = item;
        }
        ul.appendChild(li);
    });

    element.appendChild(ul);
    return ul;
}

const myArray = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
const list = createList(myArray);
document.body.appendChild(list);
let seconds = 3;
const countdown = setInterval(() => {
    seconds--;
    console.log(seconds + 1);
    if (seconds <= 0) {
        clearInterval(countdown);
        document.body.innerHTML = '';
    }
}, 1000);