function validateData(name, age) {
    // Перевірка, чи введено ім'я та вік
    return name && !isNaN(age) && age >= 1 && age <= 120;
}

function getInputData(defaultName, defaultAge) {
    let name = defaultName;
    let age = defaultAge;

    do {
        name = prompt("Введіть ваше ім'я:", name);
        age = prompt("Введіть ваш вік:", age);
    } while (!validateData(name, age));

    return { name, age: parseInt(age) };
}


const userData = getInputData("", "");
const { name, age } = userData;

if (age < 18) {
    alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
    const confirmation = confirm("Are you sure you want to continue?");
    if (confirmation) {
        alert("Welcome, " + name + "!");
    } else {
        alert("You are not allowed to visit this website.");
    }
} else {
    alert("Welcome, " + name + "!");
}
