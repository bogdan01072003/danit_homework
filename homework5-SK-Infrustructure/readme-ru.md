## Задание

Сверстать макет [Sk Infrustructure](https://www.figma.com/file/hxHQ2F6GPbdooXRLoA7tmX/HW---SK-Infrustructure?type=design&node-id=0-1&t=Ah9bYznpVBYntCpf-0).

#### Технические требования к верстке

- Кнопка обратный звонок должна быть сделана в виде ссылки
- Элементы в первой секции `Наружные тепловые сети` и `Собственный парк строительной техники` должна быть выполнена с помощью CSS Grid.
- Блоки с текстом в секциях должны быть сверстаны, а не в виде картинок. 

#### Примечание
- Верстка должна быть выполнена без использования CSS библиотек типа Bootstrap или Materialize.

#### Полезные ссылки

[CSS FlexBox](https://dan-it.gitlab.io/fe-book/programming_essentials/html_css/lesson11_flexbox/flexbox.html)

[CSS Grid](https://dan-it.gitlab.io/fe-book/programming_essentials/html_css/lesson12_grid/grid.html)

[Parallax](https://dan-it.gitlab.io/fe-book/programming_essentials/html_css/lesson13_animation_parallax/parallax.html)
