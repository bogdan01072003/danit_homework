## Завдання

Зверстати макет [Sk Infrustructure](https://www.figma.com/file/hxHQ2F6GPbdooXRLoA7tmX/HW---SK-Infrustructure?type=design&node-id=0-1&t=Ah9bYznpVBYntCpf-0).

#### Технічні вимоги до верстки

- Кнопка зворотного дзвінка має бути зроблена у вигляді посилання
- Елементи в першій секції `Зовнішні теплові мережі` та `Власний парк будівельної техніки` мають бути виконані за допомогою CSS Grid.
- Блоки з текстом у секціях мають бути зверстані, а не у вигляді картинок.

#### Примітка
- Верстка повинна бути виконана без використання бібліотек CSS типу Bootstrap або Materialize.

#### Корисні посилання

[CSS FlexBox](https://dan-it.gitlab.io/fe-book-ua/programming_essentials/html_css/lesson11_flexbox/flexbox.html)

[CSS Grid](https://dan-it.gitlab.io/fe-book-ua/programming_essentials/html_css/lesson12_grid/grid.html)

[Parallax](https://dan-it.gitlab.io/fe-book-ua/programming_essentials/html_css/lesson13_animation_parallax/parallax.html)
