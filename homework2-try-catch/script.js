const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];
let root = document.getElementById("root");

for (let i = 0; i < books.length; i++) {
    try {
        let ul = document.createElement("ul");
        if (books[i].author) {
            let li = document.createElement("li");
            li.textContent = books[i].author;
            ul.appendChild(li);

        }
        else{
            throw new SyntaxError(`Author not available in book ${i}`);
        }

        if (books[i].name) {
            let li2 = document.createElement("li");
            li2.textContent = books[i].name;
            ul.appendChild(li2);
        }
        else{
            throw new SyntaxError(`Name not available in book ${i}`);
        }

        if (books[i].price) {
            let li3 = document.createElement("li");
            li3.textContent = books[i].price;
            ul.appendChild(li3);
        }
        else{
            throw new SyntaxError(`Price not available in book ${i}`);
        }

        root.appendChild(ul);

    } catch (e) {
        console.log(e.message);
    }
}
