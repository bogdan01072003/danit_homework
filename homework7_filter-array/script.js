function filterBy(arr, dataType) {
    if (typeof dataType !== 'string') {
        throw new Error('Будь ласка, введіть коректний тип даних');
    }

    return arr.filter(function(item) {
        return typeof item !== dataType;
    });
}

const inputArray = ['hello', 'world', 23, '23', null];
const filteredArray = filterBy(inputArray, 'number');
console.log(filteredArray);
