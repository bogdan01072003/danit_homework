{let activeWindow = document.querySelector('.active-window');
function changeServiceInfo(windowNumber) {
    const serviceInfoImage = document.querySelector('.service-info img');
    const serviceInfoText = document.querySelector('.service-info span');
    const windowData = {
        window1: {
            image: './img/photo-service.png',
            text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        },
        window2: {
            image: './img/work-image1.png',
            text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        },
        window3: {
            image: './img/work-image2.png',
            text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        },
        window4: {
            image: './img/work-image3.png',
            text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        },
        window5: {
            image: './img/work-image4.png',
            text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        },
        window6: {
            image: './img/work-image5.png',
            text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        },
    };

    const data = windowData[`window${windowNumber}`];
    serviceInfoImage.src = data.image;
    serviceInfoText.textContent = data.text;
    if (activeWindow !== null) {
        activeWindow.classList.remove('active-window');
    }

    const currentWindow = document.querySelector(`.window${windowNumber}`);
    currentWindow.classList.add('active-window');
    activeWindow = currentWindow;
}

const windows = document.querySelectorAll('.service-tables-window li');
windows.forEach((window, index) => {
    window.addEventListener('click', () => {
        changeServiceInfo(index + 1);
    });
});


function addMoreObjects() {
    const originalList = document.querySelectorAll('.work-example li');
    const container = document.querySelector('.work-example');

    if (originalList.length >= 24) {
        const loadMoreBtn = document.querySelector('.load-more');
        loadMoreBtn.style.display = 'none';
        return;
    }

    const newList = Array.from(originalList).slice(0, 12);

    newList.forEach((item) => {
        container.appendChild(item.cloneNode(true));
    });

    examples = document.querySelectorAll('.work-example li');

    loadMoreBtn.style.display = 'none';
}

const loadMoreBtn = document.querySelector('.load-more');
loadMoreBtn.addEventListener('click', addMoreObjects, { once: true });

// Отримуємо всі необхідні елементи DOM
const tables = document.querySelectorAll('.work-tables li');
let examples = document.querySelectorAll('.work-example li'); // Оновлена змінна examples

function filterExamples(event) {
    const selectedClass = event.target.classList.item(0);


    tables.forEach(table => table.classList.remove('active'));
    event.target.classList.add('active');

    examples.forEach(example => {
        example.style.display = 'none';

        if (example.classList.item(1) === selectedClass || selectedClass === 'all') {
            example.style.display = 'block';
        }
    });
}

tables.forEach(table => {
    table.addEventListener('click', filterExamples);
});

filterExamples({ target: tables[0] });
}

document.addEventListener('DOMContentLoaded', function () {
    const authorWrappers = document.querySelectorAll('.reviews-roundabout .author-wrapper');
    const textReview = document.querySelector('.reviews-text-review');
    const authorName = document.querySelector('.reviews-author-name');
    const authorPosition = document.querySelector('.reviews-author-position');
    const authorPhoto = document.querySelector('.author')

    let currentIndex = 0;

    function updateContent(index) {
        const reviews = [
            { review: 'Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa', name: 'Author 1', position: 'Position 1', src: "./img/author-1.png" },
            { review: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.', name: 'Author 2', position: 'Position 2', src: "./img/author-2.png" },
            { review: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.', name: 'Hasan Ali', position: 'UX Designer', src: "./img/review-hasan.png" },
            { review: 'quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.', name: 'Author 4', position: 'Position 4', src: "./img/author-4.png" }
        ];

        textReview.textContent = reviews[index].review;
        authorName.textContent = reviews[index].name;
        authorPosition.textContent = reviews[index].position;
        authorPhoto.src = reviews[index].src;
        authorPhoto.style.width = '143px';
        authorPhoto.style.height = '143px';
    }

    function setActiveAuthor(index) {
        authorWrappers.forEach(wrapper => wrapper.classList.remove('active-author'));
        authorWrappers[index].classList.add('active-author');
    }

    updateContent(currentIndex);
    setActiveAuthor(currentIndex);

    authorWrappers.forEach((wrapper, index) => {
        wrapper.addEventListener('click', function () {
            currentIndex = index;
            updateContent(currentIndex);
            setActiveAuthor(currentIndex);
        });
    });

    document.querySelector('.roundabout-arrow').addEventListener('click', function () {
        currentIndex = (currentIndex - 1 + authorWrappers.length) % authorWrappers.length;
        updateContent(currentIndex);
        setActiveAuthor(currentIndex);
    });

    document.querySelector('.right-arrow').addEventListener('click', function () {
        currentIndex = (currentIndex + 1) % authorWrappers.length;
        updateContent(currentIndex);
        setActiveAuthor(currentIndex);
    });
});

