import favoritesReducer, { toggleFavorite, setFavorites } from './components/favoritesSlice';

describe('favorites reducer', () => {
  const initialState = { items: [] };

  it('should return the initial state', () => {
    expect(favoritesReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle toggleFavorite', () => {
    const item = { id: 1, name: 'Test Product' };
    
    let nextState = favoritesReducer(initialState, toggleFavorite(item));
    expect(nextState.items).toEqual([item]);

    nextState = favoritesReducer(nextState, toggleFavorite(item));
    expect(nextState.items).toEqual([]);
  });

  it('should handle setFavorites', () => {
    const favorites = [{ id: 1, name: 'Product 1' }, { id: 2, name: 'Product 2' }];
    const nextState = favoritesReducer(initialState, setFavorites(favorites));
    expect(nextState.items).toEqual(favorites);
  });
});
