import cartReducer, { addItemToCart, removeItemFromCart, clearCart } from './components/CartSlice';

describe('cart reducer', () => {
  const initialState = { items: [] };

  it('should return the initial state', () => {
    expect(cartReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle addItemToCart', () => {
    const item = { id: 1, name: 'Test Product' };
    const nextState = cartReducer(initialState, addItemToCart(item));
    expect(nextState.items).toEqual([item]);
  });

  it('should handle removeItemFromCart', () => {
    const item = { id: 1, name: 'Test Product' };
    const stateWithItem = { items: [item] };
    const nextState = cartReducer(stateWithItem, removeItemFromCart(item));
    expect(nextState.items).toEqual([]);
  });

  it('should handle clearCart', () => {
    const stateWithItems = { items: [{ id: 1, name: 'Test Product' }] };
    const nextState = cartReducer(stateWithItems, clearCart());
    expect(nextState.items).toEqual([]);
  });
});
