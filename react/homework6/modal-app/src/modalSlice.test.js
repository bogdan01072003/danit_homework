import modalReducer, { openModal, closeModal } from './components/ModalSlice';

describe('modal reducer', () => {
  const initialState = { isOpen: false };

  it('should return the initial state', () => {
    expect(modalReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle openModal', () => {
    const nextState = modalReducer(initialState, openModal());
    expect(nextState.isOpen).toBe(true);
  });

  it('should handle closeModal', () => {
    const stateWithOpenModal = { isOpen: true };
    const nextState = modalReducer(stateWithOpenModal, closeModal());
    expect(nextState.isOpen).toBe(false);
  });
});
