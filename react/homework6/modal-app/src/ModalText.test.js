import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';

import ModalText from './components/ModalText'; 

describe('ModalText Component', () => {
  const onCloseMock = jest.fn();
  const onConfirmMock = jest.fn();
  
  const productName = 'Тестовий товар';

  beforeEach(() => {
    jest.clearAllMocks(); 
  });

  test('renders modal with correct product name when open', () => {
    render(
      <ModalText
        isOpen={true}
        onClose={onCloseMock}
        productName={productName}
        onConfirm={onConfirmMock}
      />
    );

    expect(screen.getByText(`Додати товар"${productName}"`)).toBeInTheDocument();
    expect(screen.getByText('Ви впевнені що хочете додати цей товар до кошику')).toBeInTheDocument();
  });

  test('does not render modal when isOpen is false', () => {
    render(
      <ModalText
        isOpen={false}
        onClose={onCloseMock}
        productName={productName}
        onConfirm={onConfirmMock}
      />
    );

    expect(screen.queryByText(`Додати товар"${productName}"`)).not.toBeInTheDocument();
  });

  test('calls onClose when clicking on "Відхилити" button', () => {
    render(
      <ModalText
        isOpen={true}
        onClose={onCloseMock}
        productName={productName}
        onConfirm={onConfirmMock}
      />
    );

    fireEvent.click(screen.getByText('Відхилити'));

    expect(onCloseMock).toHaveBeenCalledTimes(1);
  });

  test('calls onConfirm when clicking on "Підтвердити" button', () => {
    render(
      <ModalText
        isOpen={true}
        onClose={onCloseMock}
        productName={productName}
        onConfirm={onConfirmMock}
      />
    );


    fireEvent.click(screen.getByText('Підтвердити'));

 
    expect(onConfirmMock).toHaveBeenCalledTimes(1);
  });

  test('calls onClose when clicking on the close button (&times;)', () => {
    render(
      <ModalText
        isOpen={true}
        onClose={onCloseMock}
        productName={productName}
        onConfirm={onConfirmMock}
      />
    );

    fireEvent.click(screen.getByText('×'));


    expect(onCloseMock).toHaveBeenCalledTimes(1);
  });

  test('prevents closing the modal when clicking inside modal content', () => {
    render(
      <ModalText
        isOpen={true}
        onClose={onCloseMock}
        productName={productName}
        onConfirm={onConfirmMock}
      />
    );

    fireEvent.click(screen.getByText(`Додати товар"${productName}"`));

    expect(onCloseMock).not.toHaveBeenCalled();
  });
});
