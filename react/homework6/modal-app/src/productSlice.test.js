import productReducer from './components/ProductSlice';
import { fetchProducts } from './components/ProductSlice';

describe('product reducer', () => {
  const initialState = { items: [], loading: false, error: null };

  it('should return the initial state', () => {
    expect(productReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle fetchProducts.pending', () => {
    const nextState = productReducer(initialState, fetchProducts.pending());
    expect(nextState.loading).toBe(true);
  });

  it('should handle fetchProducts.fulfilled', () => {
    const products = [{ id: 1, name: 'Test Product' }];
    const nextState = productReducer(initialState, fetchProducts.fulfilled(products));
    expect(nextState.loading).toBe(false);
    expect(nextState.items).toEqual(products);
  });

  it('should handle fetchProducts.rejected', () => {
    const errorMessage = 'Failed to fetch';
    const nextState = productReducer(initialState, fetchProducts.rejected({ message: errorMessage }));
    expect(nextState.loading).toBe(false);
    expect(nextState.error).toEqual(errorMessage);
  });
});
