module.exports = {
    testEnvironment: 'jest-environment-jsdom',
    setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect'],
    moduleNameMapper: {
      '\\.(css|scss)$': 'identity-obj-proxy' // Це дозволяє ігнорувати імпорти стилів
    },
    transform: {
        "^.+\\.jsx?$": "babel-jest"
      },
      testEnvironment: "jsdom"
  };
