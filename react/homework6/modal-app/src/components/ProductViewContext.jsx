import React, { createContext, useState } from 'react';

// Створюємо контекст
export const ProductViewContext = createContext();

// Створюємо провайдер, який надаватиме стан
export const ProductViewProvider = ({ children }) => {
  const [isGridView, setIsGridView] = useState(true); // true - картки, false - таблиця

  const toggleView = () => {
    setIsGridView(!isGridView);
  };

  return (
    <ProductViewContext.Provider value={{ isGridView, toggleView }}>
      {children}
    </ProductViewContext.Provider>
  );
};
