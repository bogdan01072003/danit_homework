import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import ProductCard from './ProductCard'; 
import { ProductViewContext } from './ProductViewContext'; // Імпорт контексту

function ProductList({ products }) {
  const { isGridView, toggleView } = useContext(ProductViewContext); // Використання контексту

  return (
    <div>
      <button onClick={toggleView}>
        {isGridView ? 'Switch to List View' : 'Switch to Grid View'}
      </button>
      <div className={isGridView ? 'grid-view' : 'list-view'}>
        {products.length > 0 ? (
          products.map(product => (
            <ProductCard key={product.id} product={product} />
          ))
        ) : (
          <p>Loading products...</p>
        )}
      </div>
    </div>
  );
}

ProductList.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      imageUrl: PropTypes.string.isRequired,
      sku: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
    })
  ).isRequired,
};

export default ProductList;
