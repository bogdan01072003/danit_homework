import React from 'react';
import PropTypes from 'prop-types';
import './ModalTextDelete.scss'; 

const ModalText = ({ isOpen, onClose, productName, onConfirm }) => {
  if (!isOpen) return null;

  return (
    <div className="modal-text-wrapper" onClick={onClose}>
      <div className="modal-text-content" onClick={(e) => e.stopPropagation()}>
        <button className="modal-close" onClick={onClose}>
          &times;
        </button>
        <div className="modal-text-header">Видалити товар "{productName}"</div>
        <div className="modal-text-body">Ви впевнені що хочете видалити товар з кошику?</div>
        <div className="modal-text-footer">
          <button onClick={onConfirm}>Підтвердити</button>
          <button onClick={onClose}>Відхилити</button>
        </div>
      </div>
    </div>
  );
};

ModalText.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  productName: PropTypes.string.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

export default ModalText;
