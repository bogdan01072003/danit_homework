import React, { useState } from 'react';
import Button from './components/Button';
import ModalImage from './components/ModalImage';
import ModalText from './components/ModalText';
import './App.scss';

function App() {
  const [isFirstModalOpen, setFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setSecondModalOpen] = useState(false);

  return (
    <div className="App">
      <Button onClick={() => setFirstModalOpen(true)} classNames="open-modal-button">
        Open image modal
      </Button>

      <Button onClick={() => setSecondModalOpen(true)} classNames="open-modal-button">
        Open text modal
      </Button>

      <ModalImage
        isOpen={isFirstModalOpen}
        onClose={() => setFirstModalOpen(false)}
        imageUrl={"https://www.mountaingoatsoftware.com/uploads/blog/2016-09-06-what-is-a-product-quote.png"}
      />

      <ModalText
        isOpen={isSecondModalOpen}
        onClose={() => setSecondModalOpen(false)}
        productName={"NAME"}
      />
    </div>
  );
}

export default App;
