import React from 'react';
import './ModalText.scss';

const ModalText = ({ isOpen, onClose, productName }) => {
  if (!isOpen) return null;

  return (
    <div className="modal-text-wrapper" onClick={onClose}>
      <div className="modal-text-content" onClick={(e) => e.stopPropagation()}>
        <button className="modal-close" onClick={onClose}>
          &times;
        </button>
        <div className="modal-text-header">Add Product "{productName}"</div>
        <div className="modal-text-body">Description for your product</div>
        <div className="modal-text-footer">
          <button className="favorite-button">ADD TO FAVORITE</button>
        </div>
      </div>
    </div>
  );
};

export default ModalText;
