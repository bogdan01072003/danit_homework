import React from 'react';
import './ModalImage.scss';

const ModalImage = ({ isOpen, onClose, imageUrl }) => {
  if (!isOpen) return null;

  return (
    <div className="modal-image-wrapper" onClick={onClose}>
      <div className="modal-image-content" onClick={(e) => e.stopPropagation()}>
        <button className="modal-close" onClick={onClose}>
          &times;
        </button>
        <div className="modal-image-header">Product Delete!</div>
        <div className="modal-image-body">
          <img src={imageUrl} alt="Product" />
          <p>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</p>
        </div>
        <div className="modal-image-footer">
          <button className="cancel-button" onClick={onClose}>NO, CANCEL</button>
          <button className="delete-button">YES, DELETE</button>
        </div>
      </div>
    </div>
  );
};

export default ModalImage;
