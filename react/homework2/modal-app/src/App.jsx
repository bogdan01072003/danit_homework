import React, { useEffect, useState } from 'react';
import ProductList from './components/ProductList';
import './App.scss';
import { FaShoppingCart, FaHeart } from 'react-icons/fa';

function App() {
  const [products, setProducts] = useState([]);

  const getCartCount = () => {
    const cartItems = JSON.parse(localStorage.getItem('cart')) || [];
    return cartItems.length;
  };

  const getFavoritesCount = () => {
    const favoriteItems = JSON.parse(localStorage.getItem('favorites')) || [];
    return favoriteItems.length;
  };

  useEffect(() => {
    fetch('/product.json')
      .then(response => {
        if (!response.ok) {
          throw new Error(`HTTP помилка! Статус: ${response.status}`);
        }
        return response.json();
      })
      .then(data => {
        setProducts(data);
      })
      .catch(error => console.error('Помилка при завантаженні продуктів:', error));
  }, []);

  return (
    <div className="App">
      <header className='header1'>
        <h1>Каталог товарів</h1>
        <div className="header-icons">
          <div className="cart-icon">
            <FaShoppingCart />
            <span className="cart-count">{getCartCount()}</span> {/* Рахуємо кількість елементів */}
          </div>
          <div className="favorites-icon">
            <FaHeart />
            <span className="favorites-count">{getFavoritesCount()}</span> {/* Рахуємо кількість обраних */}
          </div>
        </div>
      </header>
      <ProductList products={products} />
    </div>
  );
}

export default App;
