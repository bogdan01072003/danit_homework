import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import ModalText from './ModalText';

function ProductCard({ product }) {
  const [isFavorite, setIsFavorite] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);

  // Перевіряємо, чи є товар у списку обраних під час завантаження компонента
  useEffect(() => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    const isProductFavorite = favorites.some(fav => fav.id === product.id);
    setIsFavorite(isProductFavorite); // Встановлюємо стан обраного
  }, [product.id]);

  const addToCart = () => {
    const cartItems = JSON.parse(localStorage.getItem('cart')) || [];
    cartItems.push(product);
    localStorage.setItem('cart', JSON.stringify(cartItems));

    setIsModalOpen(false);  // Закриваємо модальне вікно після підтвердження

    // Оновлюємо кількість елементів у кошику після додавання
    document.querySelector('.cart-count').textContent = cartItems.length;
  };

  const confirmAddToCart = () => {
    setIsModalOpen(true);  // Відкриваємо модальне вікно для підтвердження
  };

  const toggleFavorite = () => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    
    if (!isFavorite) {
      // Якщо товар ще не в обраному, додаємо його
      favorites.push(product);
    } else {
      // Якщо товар вже в обраному, видаляємо його
      const index = favorites.findIndex(fav => fav.id === product.id);
      if (index > -1) {
        favorites.splice(index, 1);
      }
    }

    // Оновлюємо localStorage
    localStorage.setItem('favorites', JSON.stringify(favorites));

    // Оновлюємо стан компонента
    setIsFavorite(!isFavorite);

    // Оновлюємо кількість обраних товарів
    document.querySelector('.favorites-count').textContent = favorites.length;
  };

  return (
    <div className="product-card">
      <img src={product.imageUrl} alt={product.name} />
      <h3>{product.name}</h3>
      <p>Price: ${product.price}</p>
      <button onClick={confirmAddToCart}>Add to Cart</button>
      <span onClick={toggleFavorite}>
        {isFavorite ? '★' : '☆'} {/* Іконка зірки, яка горить, якщо товар в обраному */}
      </span>

      {/* Модальне вікно для підтвердження додавання в кошик */}
      <ModalText
        isOpen={isModalOpen}
        onClose={() => setIsModalOpen(false)}
        productName={product.name}
        onConfirm={addToCart}
      />
    </div>
  );
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imageUrl: PropTypes.string.isRequired,
    sku: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
  }).isRequired,
};

export default ProductCard;
