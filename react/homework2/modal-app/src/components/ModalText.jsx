import React from 'react';
import PropTypes from 'prop-types';
import './ModalText.scss';  // Стилі для вашого модального вікна

const ModalText = ({ isOpen, onClose, productName, onConfirm }) => {
  if (!isOpen) return null;

  return (
    <div className="modal-text-wrapper" onClick={onClose}>
      <div className="modal-text-content" onClick={(e) => e.stopPropagation()}>
        <button className="modal-close" onClick={onClose}>
          &times;
        </button>
        <div className="modal-text-header">Add Product "{productName}"</div>
        <div className="modal-text-body">
          Are you sure you want to add this product to the cart?
        </div>
        <div className="modal-text-footer">
          <button onClick={onConfirm}>Confirm</button>
          <button onClick={onClose}>Cancel</button>
        </div>
      </div>
    </div>
  );
};

ModalText.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  productName: PropTypes.string.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

export default ModalText;
