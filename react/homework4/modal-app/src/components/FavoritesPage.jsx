import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import ProductCard from './ProductCard';
import { setFavorites } from './favoritesSlice';

const FavoritesPage = () => {
  const dispatch = useDispatch();
  const favorites = useSelector((state) => state.favorites.items); 

  return (
    <div>
      <h2>Улюблені товари</h2>
      {favorites.length > 0 ? (
        favorites.map(product => (
          <ProductCard key={product.id} product={product} />
        ))
      ) : (
        <p>У вас немає улюблених товарів.</p>
      )}
    </div>
  );
}

export default FavoritesPage;
