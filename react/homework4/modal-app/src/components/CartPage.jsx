import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import ProductCard from './ProductCard';
import ModalTextDelete from './ModalTextDelete';
import { removeItemFromCart } from './CartSlice';
import { openModal, closeModal } from './ModalSlice';

const CartPage = () => {
  const dispatch = useDispatch();
  const cartItems = useSelector((state) => state.cart.items);
  const isModalOpen = useSelector((state) => state.modal.isOpen);
  const [itemToDelete, setItemToDelete] = useState(null);


  const removeFromCart = (product) => {
    dispatch(removeItemFromCart(product));
    dispatch(closeModal());
  };

  const confirmRemove = (product) => {
    setItemToDelete(product);
    dispatch(openModal());
  };

  return (
    <div>
      <h2>Твій кошик</h2>
      <div className='product-list'>
        {cartItems.length > 0 ? (
          cartItems.map(product => (
            <div key={product.id} style={{ position: 'relative' }}>
              <ProductCard product={product} />
              <button
                className='cart-button'
                style={{
                  position: 'absolute',
                  top: '12px',
                  right: '25px',
                  background: 'transparent',
                  border: 'none',
                  color: 'red',
                  fontSize: '24px',
                  cursor: 'pointer',
                }}
                onClick={() => confirmRemove(product)}
              >
                &times;
              </button>
            </div>
          ))
        ) : (
          <p>У твоєму кошику немає товарів</p>
        )}

        {isModalOpen && (
          <ModalTextDelete
            isOpen={isModalOpen}
            onClose={() => dispatch(closeModal())}
            productName={itemToDelete ? itemToDelete.name : ''}
            onConfirm={() => removeFromCart(itemToDelete)}
          />
        )}
      </div>
    </div>
  );
};

export default CartPage;
