
import { configureStore } from '@reduxjs/toolkit';
import cartReducer from './CartSlice';
import favoritesReducer from './favoritesSlice';
import modalReducer from './ModalSlice'
import productReducer from './ProductSlice'

const store = configureStore({
  reducer: {
    cart: cartReducer,
    favorites: favoritesReducer,
    modal: modalReducer,
    products: productReducer,
  },
});

export default store;
