import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import ProductList from './components/ProductList';
import CartPage from './components/CartPage';
import FavoritesPage from './components/FavoritesPage';
import './App.scss';
import { FaShoppingCart, FaHeart } from 'react-icons/fa';

function App() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch('/product.json')
      .then(response => response.json())
      .then(data => setProducts(data))
      .catch(error => console.error('Error loading products:', error));
  }, []);

  const getCartCount = () => {
    const cartItems = JSON.parse(localStorage.getItem('cart')) || [];
    return cartItems.length;
  };

  const getFavoritesCount = () => {
    const favoriteItems = JSON.parse(localStorage.getItem('favorites')) || [];
    return favoriteItems.length;
  };

  return (
    <Router>
      <div className="App">
        <header className='header1'>
          <Link to="/">
          <h1 >Каталог товарів</h1>
          </Link>
          <div className="header-icons">
            <Link to="/cart" className="cart-icon">
              <FaShoppingCart />
              <span className="cart-count">{getCartCount()}</span>
            </Link>
            <Link to="/favorites" className="favorites-icon">
              <FaHeart />
              <span className="favorites-count">{getFavoritesCount()}</span>
            </Link>
          </div>
        </header>
        <Routes>
          <Route path="/" element={<ProductList products={products} />} />
          <Route path="/cart" element={<CartPage />} />
          <Route path="/favorites" element={<FavoritesPage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
