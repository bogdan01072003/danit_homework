// CartModal.jsx
import React from 'react';
import PropTypes from 'prop-types';

function CartModal({ product, onClose, onConfirm }) {
  return (
    <div className="modal">
      <div className="modal-content">
        <h2>Confirm Add to Cart</h2>
        <p>Are you sure you want to add <strong>{product.name}</strong> to your cart?</p>
        <button onClick={onConfirm}>Confirm</button>
        <button onClick={onClose}>Cancel</button>
      </div>
    </div>
  );
}

CartModal.propTypes = {
  product: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

export default CartModal;
