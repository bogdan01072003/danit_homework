import React, { useEffect, useState } from 'react';
import ProductCard from './ProductCard'; // Імпортуйте ваш компонент картки продукту

function FavoritesPage() {
  const [favorites, setFavorites] = useState([]);

  useEffect(() => {
    const storedFavorites = JSON.parse(localStorage.getItem('favorites')) || [];
    setFavorites(storedFavorites);
  }, []);

  const removeFromFavorites = (product) => {
    const updatedFavorites = favorites.filter(item => item.id !== product.id);
    localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
    setFavorites(updatedFavorites);
  };

  return (
    <div>
      <h2>Улюблені товари</h2>
      {favorites.length > 0 ? (
        favorites.map(product => (
          <ProductCard 
            key={product.id} 
            product={product} 
            onRemoveFromFavorites={removeFromFavorites} // Передайте функцію для видалення
          />
        ))
      ) : (
        <p>У вас немає улюблених товарів.</p>
      )}
    </div>
  );
}

export default FavoritesPage;
