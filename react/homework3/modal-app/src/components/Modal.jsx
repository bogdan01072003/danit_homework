import React from 'react';
import ModalWrapper from './ModalWrapper';

const Modal = ({ children, onClose }) => {
  return (
    <ModalWrapper onClose={onClose}>
      {children}
    </ModalWrapper>
  );
};

export default Modal;
