import React, { useState, useEffect } from 'react';
import ProductCard from './ProductCard';  
import ModalText from './ModalText';
import ModalTextDelete from './ModalTextDelete';

const CartPage = () => {
  const [cartItems, setCartItems] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [itemToDelete, setItemToDelete] = useState(null);

  useEffect(() => {
    const items = JSON.parse(localStorage.getItem('cart')) || [];
    setCartItems(items);
  }, []);

  const removeFromCart = (product) => {
    const updatedCart = cartItems.filter(item => item.id !== product.id);
    localStorage.setItem('cart', JSON.stringify(updatedCart));
    setCartItems(updatedCart);
    document.querySelector('.cart-count').textContent = updatedCart.length;
    setIsModalOpen(false);
  };

  const confirmRemove = (product) => {
    setItemToDelete(product);
    setIsModalOpen(true);
  };

  return (
    <div>
      <h2>Твій кошик</h2>
      <div className='product-list'>
      {cartItems.length > 0 ? (
        cartItems.map(product => (
          <div key={product.id} style={{ position: 'relative' }}>
            <ProductCard product={product} />
            <button className='cart-button'
              style={{ 
                position: 'absolute', 
                top: '12px', 
                right: '25px', 
                background: 'transparent', 
                border: 'none', 
                color: 'red', 
                fontSize: '24px', 
                cursor: 'pointer' 
              }}
              onClick={() => confirmRemove(product)}
            >
              &times;
            </button>
          </div>
        ))
      ) : (
        <p>У твоєму кошику немає товарів</p>
      )}

      {isModalOpen && (
        <ModalTextDelete
          isOpen={isModalOpen}
          onClose={() => setIsModalOpen(false)}
          productName={itemToDelete.name}
          onConfirm={() => removeFromCart(itemToDelete)}
        />
      )}
      </div>
    </div>
  );
};

export default CartPage;
