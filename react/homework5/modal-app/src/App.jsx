import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import ProductList from './components/ProductList';
import CartPage from './components/CartPage';
import FavoritesPage from './components/FavoritesPage';
import { fetchProducts } from './components/ProductSlice';
import './App.scss';
import { FaShoppingCart, FaHeart } from 'react-icons/fa';

function App() {
  const dispatch = useDispatch();
  const cartItems = useSelector((state) => state.cart.items);
  const favoriteItems = useSelector((state) => state.favorites.items);
  const products = useSelector((state) => state.products.items);

  useEffect(() => {
    dispatch(fetchProducts()); 
  }, [dispatch]);

  const getCartCount = () => {
    return cartItems.length;
  };

  const getFavoritesCount = () => {
    return favoriteItems.length;
  };

  return (
    <Router>
      <div className="App">
        <header className='header1'>
          <Link to="/">
            <h1>Каталог товарів</h1>
          </Link>
          <div className="header-icons">
            <Link to="/cart" className="cart-icon">
              <FaShoppingCart />
              <span className="cart-count">{getCartCount()}</span>
            </Link>
            <Link to="/favorites" className="favorites-icon">
              <FaHeart />
              <span className="favorites-count">{getFavoritesCount()}</span>
            </Link>
          </div>
        </header>
        <Routes>
          <Route path="/" element={<ProductList products={products} />} />
          <Route path="/cart" element={<CartPage />} />
          <Route path="/favorites" element={<FavoritesPage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
