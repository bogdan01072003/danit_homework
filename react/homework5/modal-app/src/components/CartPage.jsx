import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import ProductCard from './ProductCard';
import ModalTextDelete from './ModalTextDelete';
import { removeItemFromCart, clearCart } from './CartSlice'; // Імпортуємо clearCart
import { openModal, closeModal } from './ModalSlice';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup'
import './Form.scss';

const CartPage = () => {
  const dispatch = useDispatch();
  const cartItems = useSelector((state) => state.cart.items);
  const isModalOpen = useSelector((state) => state.modal.isOpen);
  const [itemToDelete, setItemToDelete] = useState(null);

  const removeFromCart = (product) => {
    dispatch(removeItemFromCart(product));
    dispatch(closeModal());
  };

  const confirmRemove = (product) => {
    setItemToDelete(product);
    dispatch(openModal());
  };


  const validationSchema = Yup.object({
    firstName: Yup.string().required('Ім\'я обов\'язкове'),
    lastName: Yup.string().required('Прізвище обов\'язкове'),
    age: Yup.number().required('Вік обов\'язковий').positive().integer(),
    address: Yup.string().required('Адреса доставки обов\'язкова'),
    phone: Yup.string().required('Мобільний телефон обов\'язковий')
      .matches(/^[0-9]{10}$/, 'Мобільний телефон має містити 10 цифр'),
  });


  const handleCheckout = (values, { resetForm }) => {
    console.log('Придбані товари:', cartItems);
    console.log('Дані користувача:', values);
    dispatch(clearCart());
    localStorage.removeItem('cart');

    resetForm();

    alert('Дякуємо за покупку!');
  };

  return (
    <div>
      <h2>Твій кошик</h2>
      <div className='products-form'>
      <div className='product-list'>
        {cartItems.length > 0 ? (
          cartItems.map(product => (
            <div key={product.id} style={{ position: 'relative' }}>
              <ProductCard product={product} />
              <button
                className='cart-button'
                style={{
                  position: 'absolute',
                  top: '12px',
                  right: '25px',
                  background: 'transparent',
                  border: 'none',
                  color: 'red',
                  fontSize: '24px',
                  cursor: 'pointer',
                }}
                onClick={() => confirmRemove(product)}
              >
                &times;
              </button>
            </div>
          ))
        ) : (
          <p>У твоєму кошику немає товарів</p>
        )}

        {isModalOpen && (
          <ModalTextDelete
            isOpen={isModalOpen}
            onClose={() => dispatch(closeModal())}
            productName={itemToDelete ? itemToDelete.name : ''}
            onConfirm={() => removeFromCart(itemToDelete)}
          />
        )}
      </div>
      {cartItems.length > 0 && (
        <div className="checkout-form">
          <h3>Введіть ваші дані</h3>
          <Formik
            initialValues={{ firstName: '', lastName: '', age: '', address: '', phone: '' }}
            validationSchema={validationSchema}
            onSubmit={handleCheckout}
          >
            {({ touched, errors }) => (
              <Form>
                <div>
                  <label htmlFor="firstName">Ім'я</label>
                  <Field name="firstName" type="text" />
                  <ErrorMessage name="firstName" component="div" />
                </div>
              
                <div>
                  <label htmlFor="lastName">Прізвище</label>
                  <Field name="lastName" type="text" />
                  <ErrorMessage name="lastName" component="div" />
                </div>

                <div>
                  <label htmlFor="age">Вік</label>
                  <Field name="age" type="number" />
                  <ErrorMessage name="age" component="div" />
                </div>

                <div>
                  <label htmlFor="address">Адреса доставки</label>
                  <Field name="address" type="text" />
                  <ErrorMessage name="address" component="div" />
                </div>

                <div>
                  <label htmlFor="phone">Мобільний телефон</label>
                  <Field name="phone" type="text" />
                  <ErrorMessage name="phone" component="div" />
                </div>

                <button type="submit">Checkout</button>
              </Form>
            )}
          </Formik>
        </div>
      )}
      </div>
    </div>
  );
};

export default CartPage;
