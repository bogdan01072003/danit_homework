
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { addItemToCart } from './CartSlice';
import { toggleFavorite } from './favoritesSlice';
import ModalText from './ModalText';

function ProductCard({ product }) {
  const dispatch = useDispatch();
  const favorites = useSelector((state) => state.favorites.items);
  const isFavorite = favorites.some(fav => fav.id === product.id);

  const [isModalOpen, setIsModalOpen] = useState(false);

  const addToCart = () => {
    dispatch(addItemToCart(product)); 
    setIsModalOpen(false); 
  };

  const confirmAddToCart = () => {
    setIsModalOpen(true);  
  };

  const toggleFavoriteStatus = () => {
    dispatch(toggleFavorite(product));
  };

  return (
    <div className="product-card">
      <img src={product.imageUrl} alt={product.name} />
      <h3>{product.name}</h3>
      <p>Price: ${product.price}</p>
      <button onClick={confirmAddToCart}>Add to Cart</button>
      <span onClick={toggleFavoriteStatus}>
        {isFavorite ? '★' : '☆'}
      </span>

   
      <ModalText
        isOpen={isModalOpen}
        onClose={() => setIsModalOpen(false)}
        productName={product.name}
        onConfirm={addToCart}
      />
    </div>
  );
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imageUrl: PropTypes.string.isRequired,
    sku: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
  }).isRequired,
};

export default ProductCard;
