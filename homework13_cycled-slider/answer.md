## Теоретичні питання

1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval(`)`.
    * setTimeout() виконує передану функцію один раз після затримки
    * setInterval() виконує передану функцію регулярно кожен певний інтервал часу
2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку? Чи спрацює вона миттєво і чому?
    * не спрацює, вона спрацює одразу після завершення основного потоку
3. Чому важливо не забувати викликати функцію `clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен?
    * Він вивільнює ресурси, які використовувались під час setInterval(), тому його потрібно обов'язково викликати після виконання setInterval()