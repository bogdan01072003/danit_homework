function getNumbers() {
    let firstNumber;
    let secondNumber;

    while (true) {
        const input1 = prompt("Введіть перше число:");
        const input2 = prompt("Введіть друге число:");

        firstNumber = parseFloat(input1);
        secondNumber = parseFloat(input2);

        if (!isNaN(firstNumber) && !isNaN(secondNumber)) {
            break;
        } else {
            alert("Будь ласка, введіть коректні числа.");
        }
    }

    return [firstNumber, secondNumber];
}

function performOperation() {
    const [firstNumber, secondNumber] = getNumbers();
    const operation = prompt("Введіть математичну операцію (+, -, *, /):");

    let result;

    switch (operation) {
        case '+':
            result = firstNumber + secondNumber;
            break;
        case '-':
            result = firstNumber - secondNumber;
            break;
        case '*':
            result = firstNumber * secondNumber;
            break;
        case '/':
            if (secondNumber === 0) {
                alert("Ділення на нуль не допускається.");
                return;
            }
            result = firstNumber / secondNumber;
            break;
        default:
            alert("Неправильна операція. Введіть +, -, *, або /.");
            return;
    }

    console.log(`Результат операції: ${result}`);
}

performOperation();
