let number;

do {
    const userInput = prompt("Будь ласка, введіть ціле число:");
    number = parseFloat(userInput);

    if (!Number.isInteger(number)) {
        console.log("Ви ввели неціле число. Будь ласка, спробуйте ще раз.");
    }
} while (!Number.isInteger(number));

if (number % 5 === 0) {
    for (let i = 0; i <= number; i += 5) {
        if(i === 0) continue;
        console.log(i);
    }
} else {
    console.log("Sorry, no numbers");
}